import app from "./styles/App.module.scss";
import { Carousel } from "./components/Carousel/Carousel";
import { Header } from "./components/Header/Header";
import Tablet from "./assets/ipad.png";
import { FaApple, FaTabletAlt, FaRegFolder, FaCode, FaRegEnvelope, FaRegBookmark } from "react-icons/fa";
import { DiAndroid, DiWindows } from "react-icons/di";
import {RiLeafLine} from "react-icons/ri"
import { Section } from "./components/Section/Section";
import { Footer } from "./components/Footer/Footer";

function App() {
  return (
    <>
      <div className={app.topo}>
        <Header />

        <div className="container">
          <Carousel>
            <div className="carousel-wrapper">
              <div className="carousel-col-left">
                <h1 className="titulo">Inspire your inspiration</h1>

                <h2 className="subtitulo">
                  Simple to use for your app, products showcase and your
                  inspiration
                </h2>

                <p>
                  Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                  Tempore, et? Alias assumenda, obcaecati iste neque quisquam
                  animi totam itaque nisi veritatis, ut minima optio cum
                  pariatur asperiores, exercitationem quis veniam!
                </p>

                <div className="carousel-icons-wrapper">
                  <span>
                    {" "}
                    <FaApple size={40} />
                  </span>
                  <span>
                    {" "}
                    <DiAndroid size={40} />
                  </span>
                  <span>
                    {" "}
                    <DiWindows size={40} />
                  </span>
                </div>
              </div>

              <div className="carousel-imagem">
                <img src={Tablet} alt="#" />
              </div>
            </div>

            <div className="carousel-wrapper">
              <div className="carousel-col-left">
                <h1 className="titulo">Inspire your inspiration</h1>

                <h2 className="subtitulo">
                  Simple to use for your app, products showcase and your
                  inspiration
                </h2>

                <p>
                  Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                  Tempore, et? Alias assumenda, obcaecati iste neque quisquam
                  animi totam itaque nisi veritatis, ut minima optio cum
                  pariatur asperiores, exercitationem quis veniam!
                </p>

                <div className="carousel-icons-wrapper">
                  <span>
                    {" "}
                    <FaApple size={40} />
                  </span>
                  <span>
                    {" "}
                    <DiAndroid size={40} />
                  </span>
                  <span>
                    {" "}
                    <DiWindows size={40} />
                  </span>
                </div>
              </div>

              <div className="carousel-imagem">
                <img src={Tablet} alt="#" />
              </div>
            </div>

            <div className="carousel-wrapper">
              <div className="carousel-col-left">
                <h1 className="titulo">Inspire your inspiration</h1>

                <h2 className="subtitulo">
                  Simple to use for your app, products showcase and your
                  inspiration
                </h2>

                <p>
                  Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                  Tempore, et? Alias assumenda, obcaecati iste neque quisquam
                  animi totam itaque nisi veritatis, ut minima optio cum
                  pariatur asperiores, exercitationem quis veniam!
                </p>

                <div className="carousel-icons-wrapper">
                  <span>
                    {" "}
                    <FaApple size={40} />
                  </span>
                  <span>
                    {" "}
                    <DiAndroid size={40} />
                  </span>
                  <span>
                    {" "}
                    <DiWindows size={40} />
                  </span>
                </div>
              </div>

              <div className="carousel-imagem">
                <img src={Tablet} alt="#" />
              </div>
            </div>
          </Carousel>
        </div>
      </div>

      <div className="container">
        <Section
          title="Tinyone features"
          subtitle="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus tristique bibendum. Donec rutrum sed sem quis venenatis."
        >
          <div className={app.cardsWrapper}>
              <div className={app.card}>
                <div className={app.icon}>
                  <FaTabletAlt size={30} />
                </div>
                <div>
                  <h4 className={`${app.title} center-mobile`}>Fully Responsive</h4>
                  <p className={`${app.text} center-mobile`}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus tristique bibendum.
                  </p>
                </div>
              </div>

              <div className={app.card}>
                <div className={app.icon}>
                  <RiLeafLine size={30} />
                </div>
                <div>
                  <h4 className={`${app.title} center-mobile`}>Fully Layered PSD</h4>
                  <p className={`${app.text} center-mobile`}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus tristique bibendum.
                  </p>
                </div>
              </div>

              <div className={app.card}>
                <div className={app.icon}>
                  <FaRegFolder size={30} />
                </div>
                <div>
                  <h4 className={`${app.title} center-mobile`}>FontAwesome Icons</h4>
                  <p className={`${app.text} center-mobile`}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus tristique bibendum.
                  </p>
                </div>
              </div>

              <div className={app.card}>
                <div className={app.icon}>
                  <FaCode size={30} />
                </div>
                <div>
                  <h4 className={`${app.title} center-mobile`}>HTML5 & CSS3</h4>
                  <p className={`${app.text} center-mobile`}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus tristique bibendum.
                  </p>
                </div>
              </div>

              <div className={app.card}>
                <div className={app.icon}>
                  <FaRegEnvelope size={30} />
                </div>
                <div>
                  <h4 className={`${app.title} center-mobile`}>Email Template</h4>
                  <p className={`${app.text} center-mobile`}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus tristique bibendum.
                  </p>
                </div>
              </div>

              <div className={app.card}>
                <div className={app.icon}>
                  <FaRegBookmark size={30} />
                </div>
                <div>
                  <h4 className={`${app.title} center-mobile`}>Free to download</h4>
                  <p className={`${app.text} center-mobile`}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus tristique bibendum.
                  </p>
                </div>
              </div>
          </div>
        </Section>
      </div>

      <Footer />
    </>
  );
}

export default App;
