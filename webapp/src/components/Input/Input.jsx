import input from "./Input.module.scss"

export const Input = ({placeholder, value, setValue, width}) => {
    const style = {
       width
    }
    return (
        <input style={style}  className={input.input} placeholder={placeholder} type="text" value={value} onChange={({target}) => setValue(target.value)} />
    )
}