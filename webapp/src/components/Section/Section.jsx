import "../../styles/Global.scss"
import section from "./Section.module.scss"


export const Section = ({title, subtitle, children}) => {
    return (
        <section className={section.section}>
            <h2 className="titulo center">{title}</h2>
            <h3 className="subtitulo center">{subtitle}</h3>

            {children}
        </section>
    )
}