
import "./Carousel.scss";
import Slider from "react-slick";


export const Carousel = ({children}) => {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 5000
      };
  return (
    <Slider {...settings}>
        {children}
    </Slider>
  );
};
