import "../../styles/Global.scss";
import styles from "./Header.module.scss";
import Logo from "../../assets/logo.png";
import { FiMenu } from "react-icons/fi";
import { useEffect, useLayoutEffect, useState } from "react";

export const Header = () => {
  const [menuMobile, setMenuMobile] = useState(false);
  const [size, setSize] = useState([0, 0]);

  useLayoutEffect(() => {
    function updateSize() {
      setSize([window.innerWidth, window.innerHeight]);
    }
    window.addEventListener("resize", updateSize);
    updateSize();
    return () => window.removeEventListener("resize", updateSize);
  }, []);

  useEffect(() => {
    if (size[0] >= 765) {
      setMenuMobile(true);
    } else{
        setMenuMobile(false)
    }
  }, [size]);

  return (
    <header className={styles.header}>
      <div className="container">
        <div className={styles.logo}>
          <img src={Logo} alt="Tinyone" />
        </div>

        <div
          onClick={() => setMenuMobile(!menuMobile)}
          className={styles.iconMenu}
        >
          <FiMenu size={40} />
        </div>

        <nav className={`${styles.menu} ${menuMobile ? "active" : ""}`}>
          <a href="#">Features</a>
          <a href="#">Support</a>
          <a href="#">Blog</a>
        </nav>
      </div>
    </header>
  );
};
