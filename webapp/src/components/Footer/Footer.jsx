import { useState } from "react";
import "../../styles/Global.scss";
import footer from "./Footer.module.scss";
import { Input } from "../Input/Input";
import { Button } from "../Button/Button";
import { AiFillFacebook } from "react-icons/ai";
import { FaTwitterSquare, FaPinterestSquare } from "react-icons/fa";
import { ImGooglePlus2 } from "react-icons/im";

export const Footer = () => {
  const [email, setEmail] = useState("");
  return (
    <footer className={footer.footer}>
      <div className="container">
        <div className={footer.wrapper}>
          <h2 className={`center title ${footer.title}`}>
            Keep in touch with us
          </h2>

          <p className={`center ${footer.text}`}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent
            vitae eros eget tellus tristique bibendum. Donec rutrum sed sem quis
            venenatis.
          </p>

          <div className={footer.form}>
            <Input
              placeholder="Enter your email to update"
              value={email}
              setValue={setEmail}
            />

            <Button>Submit</Button>
          </div>

          <div className={footer.redesSociais}>
            <a href="#">
              <AiFillFacebook size={32} color="#fcdb00" />
            </a>

            <a href="#">
              <FaTwitterSquare size={32} color="#fcdb00" />
            </a>

            <a href="#">
              <ImGooglePlus2 size={30} color="#fcdb00" />
            </a>

            <a href="#">
              <FaPinterestSquare size={32} color="#fcdb00" />
            </a>
          </div>
        </div>

        <div className={footer.links}>
          <div>
            <address>
              HALOVIETNAM LTD <br/> 66, Dang Van ngu, Dong Da <br/>Hanoi, Vietnam
            </address>

            <a href="malito:contact@halovietnam.com">contact@halovietnam.com</a>

            <p>+844 35149182</p>
          </div>

          <div>
            <a href="#"> Examples</a>
            <a href="#">Shop</a>
            <a href="#">License</a>
          </div>

          <div>
            <a href="#">Contact</a>
            <a href="#">About</a>
            <a href="#">Privacy</a>
            <a href="#">Terms</a>
          </div>

          <div>
            <a href="#"> Download</a>
            <a href="#">Support</a>
            <a href="#">Documents</a>
          </div>

          <div>
            <a href="#">Media</a>
            <a href="#">Blog</a>
            <a href="#">Forums</a>
          </div>
        </div>
      </div>
    </footer>
  );
};
