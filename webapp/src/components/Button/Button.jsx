import button from "./Button.module.scss"

export const Button = ({children}) => {
    return (
        <button className={button.button}>{children}</button>
    )
}